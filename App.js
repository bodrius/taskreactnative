import React from 'react';
import 'react-native-gesture-handler';
import {Provider} from 'react-redux';
import {NavigationContainer} from '@react-navigation/native';

import configureStore from './src/redux/store';
import MainNavigation from './src/navigation/MainNavigation';

const store = configureStore();

const App = () => {
  return (
    <NavigationContainer>
      <Provider store={store}>
        <MainNavigation />
      </Provider>
    </NavigationContainer>
  );
};

export default App;
