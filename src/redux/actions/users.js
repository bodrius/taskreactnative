import {GET_USERS, PUT_USERS} from '../constants/users';

export const getUsers = () => ({
  type: GET_USERS,
});

export const putUsers = (users) => ({
  type: PUT_USERS,
  payload: users,
});
