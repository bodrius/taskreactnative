import {
  GET_FOLLOWERS,
  PUT_FOLLOWERS,
  RESET_FOLLOWERS,
} from '../constants/followers';

export const getFollowers = (userName) => ({
  type: GET_FOLLOWERS,
  payload: userName,
});

export const putFollowers = (followers) => ({
  type: PUT_FOLLOWERS,
  payload: followers,
});

export const resetFollowers = () => ({
  type: RESET_FOLLOWERS,
});
