import {PUT_USERS} from '../constants/users';

const initialState = [];

export default (state = initialState, action) => {
  switch (action.type) {
    case PUT_USERS:
      return action.payload;
    default:
      return state;
  }
};
