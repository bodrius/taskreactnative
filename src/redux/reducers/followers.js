import {PUT_FOLLOWERS, RESET_FOLLOWERS} from '../constants/followers';

const initialState = [];

export default (state = initialState, action) => {
  switch (action.type) {
    case PUT_FOLLOWERS:
      return action.payload;
    case RESET_FOLLOWERS:
      return initialState;
    default:
      return state;
  }
};
