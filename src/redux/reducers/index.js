import {combineReducers} from 'redux';
import users from './users';
import followers from './followers';

const rootReducer = combineReducers({
  users,
  followers,
});

export default rootReducer;
