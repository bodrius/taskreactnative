import React, {useEffect} from 'react';
import {View, FlatList} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';

import {getUsers} from '../../redux/actions/users';
import UserRow from '../../components/UserRow/UserRow';
import {styles} from './stylesUsersScreen';
import Loader from '../../components/Loader/Loader';

const UsersScreen = () => {
  const users = useSelector((state) => state.users);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getUsers());
  }, []);

  return (
    <View style={styles.wrapper}>
      {!!users.length ? (
        <FlatList
          data={users}
          keyExtractor={(item) => item.id.toString()}
          renderItem={({item}) => <UserRow item={item} />}
        />
      ) : (
        <Loader />
      )}
    </View>
  );
};

export default UsersScreen;
