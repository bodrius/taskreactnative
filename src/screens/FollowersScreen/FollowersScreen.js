import React, {useEffect} from 'react';
import {View, FlatList} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';

import {getFollowers, resetFollowers} from '../../redux/actions/followers';
import UserRow from '../../components/UserRow/UserRow';
import Loader from '../../components/Loader/Loader';
import {styles} from './stylesFollowersScreen';

const FollowersScreen = (params) => {
  const followers = useSelector((state) => state.followers);
  const {route} = params;
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getFollowers(route?.params?.login));
    return () => {
      dispatch(resetFollowers());
    };
  }, []);

  return (
    <>
      <View style={styles.wrapper}>
        {!!followers.length ? (
          <FlatList
            data={followers}
            keyExtractor={(item) => item.id.toString()}
            renderItem={({item}) => <UserRow item={item} />}
          />
        ) : (
          <Loader />
        )}
      </View>
    </>
  );
};

export default FollowersScreen;
