import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  wrapper: {
    flexDirection: 'row',
    paddingVertical: 5,
    justifyContent: 'space-between',
    borderWidth: 1,
    borderRadius: 10,
    borderColor: 'black',
    marginBottom: 20,
    marginHorizontal: 5,
  },
  containerLogin: {
    width: '20%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  textLogin: {
    width: '80%',
    textAlign: 'left',
    fontWeight: '700',
  },
  containerProfileLink: {
    width: '50%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  textProfileLink: {
    textAlign: 'left',
    width: '90%',
  },
  containerAvatar: {
    width: '30%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  avatar: {
    width: 100,
    height: 100,
  },
});
