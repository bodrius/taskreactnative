import React from 'react';
import {View, Text, TouchableOpacity, Image} from 'react-native';
import {useNavigation} from '@react-navigation/native';

import {styles} from './stylesUserRow';

const UserRow = ({item}) => {
  const {login, html_url, avatar_url, followers_url} = item;
  const navigation = useNavigation();

  const checkFollowers = () => {
    navigation.navigate('Followers', {followers_url, login});
  };

  return (
    <TouchableOpacity onPress={checkFollowers} style={styles.wrapper}>
      <View style={styles.containerLogin}>
        <Text numberOfLines={1} ellipsizeMode="tail" style={styles.textLogin}>
          {login}
        </Text>
      </View>

      <View style={styles.containerProfileLink}>
        <Text
          style={styles.textProfileLink}
          numberOfLines={1}
          ellipsizeMode="tail">
          {html_url}
        </Text>
      </View>

      <View style={styles.containerAvatar}>
        <Image
          style={styles.avatar}
          source={{
            uri: avatar_url,
          }}
        />
      </View>
    </TouchableOpacity>
  );
};

export default UserRow;
