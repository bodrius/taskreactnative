import React from 'react';
import {ActivityIndicator} from 'react-native';

const Loader = () => {
  return <ActivityIndicator size="large" color="#00bfff" />;
};

export default Loader;
