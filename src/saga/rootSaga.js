import {all, fork} from 'redux-saga/effects';
import getUsers from './getUsersSaga';
import getFollowers from './followersSaga';

export default function* rootSaga() {
  yield all([fork(getUsers), fork(getFollowers)]);
}
