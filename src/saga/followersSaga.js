import {call, put, takeEvery} from 'redux-saga/effects';
import {putFollowers} from '../redux/actions/followers';
import {requests} from '../services/request';
import {GET_FOLLOWERS} from '../redux/constants/followers';

function* usersSagaWorker({payload}) {
  try {
    const {data} = yield call(requests.getFollowers, payload);
    yield put(putFollowers(data));
  } catch (error) {
    console.log('error', error);
  }
}

export default function* usersSagaWatcher() {
  yield takeEvery(GET_FOLLOWERS, usersSagaWorker);
}
