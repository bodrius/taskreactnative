import {call, put, takeEvery} from 'redux-saga/effects';
import {putUsers} from '../redux/actions/users';
import {requests} from '../services/request';
import {GET_USERS} from '../redux/constants/users';

function* usersSagaWorker() {
  try {
    const {data} = yield call(requests.getUsers);
    yield put(putUsers(data));
  } catch (error) {
    console.log('error', error);
  }
}

export default function* usersSagaWatcher() {
  yield takeEvery(GET_USERS, usersSagaWorker);
}
