import React from 'react';

import {createStackNavigator} from '@react-navigation/stack';
import FollowersScreen from '../screens/FollowersScreen/FollowersScreen';
import UsersScreen from '../screens/UsersScreen/UsersScreen';

const Stack = createStackNavigator();

const MainNavigation = () => {
  return (
    <>
      <Stack.Navigator>
        <Stack.Screen name="Users" component={UsersScreen} />
        <Stack.Screen name="Followers" component={FollowersScreen} />
      </Stack.Navigator>
    </>
  );
};

export default MainNavigation;
