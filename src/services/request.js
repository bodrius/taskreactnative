import axios from 'axios';
import {BASE_URL} from 'react-native-dotenv';

export const requests = {
  getUsers: async () => {
    try {
      const data = await axios.get(`${BASE_URL}/users`);
      return data;
    } catch (error) {
      return error;
    }
  },
  getFollowers: async (userName) => {
    try {
      const data = await axios.get(`${BASE_URL}/users/${userName}/followers`);
      return data;
    } catch (error) {
      console.log('error', error);
      return error;
    }
  },
};
